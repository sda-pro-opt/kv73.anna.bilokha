Документація
------------

Програма була створена в Visual Studio 2017
Щоб її запустити, необхідно створити проект та добавити файл Source.cpp, знаходиться в directory - code
В main необхідно вказати path, де знаходиться файл input.

Файл input створюється - by user, generated - by program, after compilation
input має розширення - .sig

Range for program:
Ключові слова - 300..399 (в нашому випадку 300..304)
Цифри - 400..499
Ідентифікатори - 500 і тд.
Роздільники - (int)char - тобто таблиці ASCII

Символи, що можна використати в програмі:
; ( ) , *

Роздільники, або ж символи можуть бути як і без пробілу з ідентифікатором , або числами, або ключовими словами, 
проте буде вважатись що це інший token.

