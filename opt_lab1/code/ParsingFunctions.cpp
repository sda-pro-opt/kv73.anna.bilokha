#include <iostream>
#include <fstream> 
#include "ParsingFunctions.h"


void parsingStr(char str, int position, ofstream &fileoutput) {
	string ch;
	if (str != ' ')
		words += toupper(str);
	if ((words[0] >= '0' && words[0] <= '9')) {
		forNumb += str;
		words.clear();
	}
	ch += str;
	if (findSeparator(ch, separator)) {
		if (words.length() != 1) {
			if (!words.empty()) words.erase(words.end() - 1);
			checkCorrectIdentifer(words, forNumb, arrayStr, position, fileoutput);
		}
		if (str == '(') {
			symbol = true;
			return;
		}
		arrayStr.push_back({ countLine, position , (int)str, ch });
		fileoutput << countLine << '\t' << position << '\t' << (int)str << '\t' << ch << endl;
	}
	if (str == ' ') {
		checkCorrectIdentifer(words, forNumb, arrayStr, position, fileoutput);
		if (forNumb.length() != 0) {
			int get = getCode(forNumb, 1);
			arrayStr.push_back({ countLine, position - (int)forNumb.length(), get , forNumb });
			fileoutput << countLine << '\t' << position - (int)forNumb.length() << '\t' << get << '\t' << forNumb << endl;
			forNumb.clear();
		}
	}
}

void parsingFile(string namefile, string path) {
	ifstream file(path + namefile);
	ofstream fileoutput(path + "generated.txt");
	char ch, previousCh = NULL;
	int position = 0, positionEnd = 0;
	while ((ch = file.get()) != EOF) {
		position += 1;
		positionEnd = position;
		Lexer(ch, position, fileoutput);
		if (ch != '\n') {
			if ((symbol) && (ch == '*')) {
				int commentPosition = position - 1, commentLine = countLine;
				if ((ch = file.get()) != EOF) position += 1;
				while ((ch = file.get()) != EOF) {
					position += 1;
					if (previousCh == '*' && ch == ')') {
						symbol = false;
						words.clear();
						forNumb.clear();
						break;
					}
					if (ch == '\n') {
						countLine += 1;
						position = 0;
					}
					previousCh = ch;
				}
				if (symbol)
					fileoutput << "Lexer: Error(line " << commentLine << ", column " << commentPosition << ") : Comment doesn't close ";
				continue;
			}
			if (!symbol)
				parsingStr(ch, position, fileoutput);
		}
		else {
			checkCorrectIdentifer(words, forNumb, arrayStr, position, fileoutput);
			if (forNumb.length() != 0) {
				int get = getCode(forNumb, 1);
				arrayStr.push_back({ countLine, position - (int)forNumb.length() + 1, get , forNumb });
				fileoutput << countLine << '\t' << position - (int)forNumb.length() + 1 << '\t' << get << '\t' << forNumb << endl;
				forNumb.clear();
			}
			countLine += 1;
			position = 0;
		}
	}
	forEndFile(positionEnd + 1, fileoutput);
	file.close();
	fileoutput.close();
	return;
}

void forEndFile(int positionEnd, ofstream &fileoutput) {
	if (!words.empty() && !findSeparator(words, separator))
		checkCorrectIdentifer(words, forNumb, arrayStr, positionEnd, fileoutput);
	if (forNumb.length() != 0) {
		int get = getCode(forNumb, 1);
		arrayStr.push_back({ countLine, positionEnd - (int)forNumb.length() - (int)words.length(), get , forNumb });
		fileoutput << countLine << '\t' << positionEnd - (int)forNumb.length() - (int)words.length() << '\t' << get << '\t' << forNumb << endl;
		forNumb.clear();
	}
}