#pragma once
#include <iostream>
#include <optional>
#include <string>
#include <vector>
#include <map>
#include <algorithm>

using namespace std;

static map<string, int> identifer, number;
static string words, forNumb;

struct MyVector
{
	int NumberLine, Position, code;
	string wordInLine;
};

static vector<MyVector> arrayStr;

static const map<string, int>	Keywords = { { "PROGRAM",300 },{ "PROCEDURE",301 },{ "BEGIN",302 },{ "END",303 },{ "LABEL",304 } };
static const vector<string> separator = { ",", ";", "(", ")", "*" };
static bool symbol = false;
static int countLine = 1, countForIdentifer = 500, countForNumber = 400;

optional<int> findInMap(const string word);
int getCode(const string& word, bool Number);
void Lexer(char str, int position, ofstream &fileoutput);
void checkCorrectIdentifer(string& words, string& forNumb, vector<MyVector> arrayStr, int i, ofstream &fileoutput);
bool findSeparator(string words, const vector<string> &name);
