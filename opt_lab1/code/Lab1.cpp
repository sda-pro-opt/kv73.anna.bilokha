#include <iostream>
#include <fstream> 
#include "Lab1.h"

using namespace std;

extern const map<string, int>	Keywords;
extern const vector<string> separator;
extern bool symbol;
extern int countLine, countForIdentifer, countForNumber;

optional<int> findInMap(const string word)
{
	auto it = Keywords.find(word);
	if (it != Keywords.end())
		return it->second;
	return nullopt;
}


int getCode(const string& word, bool Number)
{
	if (auto result = findInMap(word))
		return *result;
	else {
		if (Number) {
			number.insert({ word, countForNumber });
			countForNumber += 1;
			return countForNumber;
		}
		else {
			identifer.insert({ word, countForIdentifer });
			countForIdentifer += 1;
			return countForIdentifer;
		}
	}
	return 1;
}

bool findSeparator(string words, const vector<string> &name) {
	for (const string &n : name)
		if (n == words) return true;
	return false;
}


void checkCorrectIdentifer(string& words, string& forNumb, vector<MyVector> arrayStr, int i, ofstream &fileoutput) {
	if (words.length() != 0) {
		int position;
		if (forNumb.length() != 0) {
			position = i - (int)forNumb.length() - (int)words.length();
			if (position < 0)
				position = 1;
			fileoutput << "Lexer: Error(line " << countLine << ", column " << position << ") : Not Correct Name for Identifer : " << forNumb << words;
			fileoutput.close();
			exit(0);
		}
		int get = getCode(words, 0);
		position = i - (int)words.length();
		if (position < 0) position = 1;
		arrayStr.push_back({ countLine, position, get , words });
		fileoutput << countLine << '\t' << position << '\t' << get << '\t' << words << endl;
		words.clear();
	}
}

void Lexer(char str, int position, ofstream &fileoutput) {
	string word;
	word += str;
	if (!((((int)str >= 65) && ((int)str <= 90)) || (((int)str >= 97) && ((int)str <= 122)) || (((int)str >= 48) && ((int)str <= 57)) || ((int)str == '\n') || ((int)str == ' ')
		|| findSeparator(word, separator))) {
		fileoutput << "Lexer: Error(line " << countLine << ", column " << position << ") : Illegal symbol ' " << str << " '";
		fileoutput.close();
		exit(0);
	}
}